## Damage to terrestrial ecosystems, land transformation

The fraction of species richness that may be potentially lost in terrestrial ecosystems due to land transformation. See [lc-impact.eu](https://lc-impact.eu/EQland_stress.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsLandTransformation](https://hestia.earth/term/damageToTerrestrialEcosystemsLandTransformation)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [chaudharyBrooks2018](https://hestia.earth/term/chaudharyBrooks2018)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) and either:
      - [ecoregion](https://hestia.earth/schema/Site#ecoregion)
      - a [country](https://hestia.earth/schema/Site#country) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
  - optional:
    - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
      - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromForest20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromForest20YearAverageDuringCycle)
      - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromOtherNaturalVegetation20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromOtherNaturalVegetation20YearAverageDuringCycle)

### Lookup used

Different lookup files are used depending on the situation:

- [ecoregion-siteType-LandTransformationChaudaryBrooks2018CF.csv](https://hestia.earth/glossary/lookups/ecoregion-siteType-LandTransformationChaudaryBrooks2018CF.csv) -> using `ecoregion`
- [region-siteType-LandTransformationChaudaryBrooks2018CF.csv](https://hestia.earth/glossary/lookups/region-siteType-LandTransformationChaudaryBrooks2018CF.csv) -> using `country`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.chaudharyBrooks2018 import run

print(run('damageToTerrestrialEcosystemsLandTransformation', ImpactAssessment))
```
