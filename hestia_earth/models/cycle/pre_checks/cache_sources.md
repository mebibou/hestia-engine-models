## Pre Checks Cache Sources

This model caches the sources of all models.

### Returns

* A [Cycle](https://hestia.earth/schema/Cycle)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run(Cycle))
```
