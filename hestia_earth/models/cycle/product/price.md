## Product Price

Sets the `price` of products to `0` in specific conditions: if the `economicValueShare` is `0`, or for `excreta`.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [price](https://hestia.earth/schema/Product#price)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('product.price', Cycle))
```
