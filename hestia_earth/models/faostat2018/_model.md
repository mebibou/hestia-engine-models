# FAOSTAT (2018)

These models uses data from the [FAOSTAT](http://www.fao.org/faostat/en/) database (accessed in 2018) to gap fill values like seed based on crop yield.
