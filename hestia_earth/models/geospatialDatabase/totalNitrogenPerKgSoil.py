from hestia_earth.schema import MeasurementMethodClassification
from hestia_earth.utils.tools import safe_parse_float

from hestia_earth.models.log import logRequirements, logShouldRun
from hestia_earth.models.utils.measurement import _new_measurement
from hestia_earth.models.utils.source import get_source
from .utils import download, has_geospatial_data, should_download
from . import MODEL

REQUIREMENTS = {
    "Site": {
        "or": [
            {"latitude": "", "longitude": ""},
            {"boundary": {}},
            {"region": {"@type": "Term", "termType": "region"}}
        ]
    }
}
RETURNS = {
    "Measurement": [{
        "value": "",
        "depthUpper": "0",
        "depthLower": "50",
        "methodClassification": "geospatial dataset"
    }]
}
TERM_ID = 'totalNitrogenPerKgSoil'
EE_PARAMS = {
    'collection': 'T_N',
    'ee_type': 'raster',
    'reducer': 'mean'
}
BIBLIO_TITLE = 'Harmonized soil property values for broad-scale modelling (WISE30sec) '
'with estimates of global soil carbon stocks'


def _measurement(site: dict, value: float):
    measurement = _new_measurement(TERM_ID)
    measurement['value'] = [value]
    measurement['depthUpper'] = 0
    measurement['depthLower'] = 50
    measurement['methodClassification'] = MeasurementMethodClassification.GEOSPATIAL_DATASET.value
    return measurement | get_source(site, BIBLIO_TITLE)


def _run(site: dict):
    value = download(TERM_ID, site, EE_PARAMS)
    return [_measurement(site, safe_parse_float(value))] if value is not None else []


def _should_run(site: dict):
    contains_geospatial_data = has_geospatial_data(site)
    below_max_area_size = should_download(TERM_ID, site)

    logRequirements(site, model=MODEL, term=TERM_ID,
                    contains_geospatial_data=contains_geospatial_data,
                    below_max_area_size=below_max_area_size)

    should_run = all([contains_geospatial_data, below_max_area_size])
    logShouldRun(site, MODEL, TERM_ID, should_run)
    return should_run


def run(site: dict): return _run(site) if _should_run(site) else []
