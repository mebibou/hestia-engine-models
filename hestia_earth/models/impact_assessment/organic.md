## Organic

Detects if the [Cycle](https://hestia.earth/schema/ImpactAssessment#cycle) has an organic label.

### Returns

- `true` if the `Cycle` has an organic label, `false` otherwise

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
      - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [standardsLabels](https://hestia.earth/glossary?termType=standardsLabels)

### Lookup used

- [standardLabels.csv](https://hestia.earth/glossary/lookups/standardLabels.csv) -> `isOrganic`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('organic', ImpactAssessment))
```
