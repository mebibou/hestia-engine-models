## Organic carbon (per ha)

The stock of organic carbon in the soil.

The IPCC Tier 2 model for estimating soil organic carbon stock changes in the 0 - 30cm depth interval for croplands
remaining croplands. Source:
[IPCC 2019, Vol. 4, Chapter 10](https://www.ipcc-nggip.iges.or.jp/public/2019rf/pdf/4_Volume4/19R_V4_Ch05_Cropland.pdf).

Currently, the Tier 2 implementation does not take into account the irrigation of cycles when estimating soil organic
carbon stock changes. This model is planned be extended to with an implementation of the the IPCC Tier 1 model.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [organicCarbonPerHa](https://hestia.earth/term/organicCarbonPerHa)
  - [methodModel](https://hestia.earth/schema/Measurement#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [dates](https://hestia.earth/schema/Measurement#dates)
  - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper) with `0`
  - [depthLower](https://hestia.earth/schema/Measurement#depthLower) with `30`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `tier 2 model`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland`
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [sandContent](https://hestia.earth/term/sandContent) and optional:
      - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper) with `0`
      - [depthLower](https://hestia.earth/schema/Measurement#depthLower) with `30`
      - [dates](https://hestia.earth/schema/Measurement#dates)
    - [value](https://hestia.earth/schema/Measurement#value) and [dates](https://hestia.earth/schema/Measurement#dates) and [term](https://hestia.earth/schema/Measurement#term) with [temperatureMonthly](https://hestia.earth/term/temperatureMonthly)
    - [value](https://hestia.earth/schema/Measurement#value) and [dates](https://hestia.earth/schema/Measurement#dates) and [term](https://hestia.earth/schema/Measurement#term) with [precipitationMonthly](https://hestia.earth/term/precipitationMonthly)
    - [value](https://hestia.earth/schema/Measurement#value) and [dates](https://hestia.earth/schema/Measurement#dates) and [term](https://hestia.earth/schema/Measurement#term) with [potentialEvapotranspirationMonthly](https://hestia.earth/term/potentialEvapotranspirationMonthly)
  - optional:
    - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [dates](https://hestia.earth/schema/Measurement#dates) and [depthUpper](https://hestia.earth/schema/Measurement#depthUpper) with `0` and [depthLower](https://hestia.earth/schema/Measurement#depthLower) with `30` and [term](https://hestia.earth/schema/Measurement#term) with [ organicCarbonPerHa](https://hestia.earth/term/ organicCarbonPerHa)
  - related to:
    - a list of [Cycles](https://hestia.earth/schema/Cycle) with:
      - [endDate](https://hestia.earth/schema/Cycle#endDate) and a list of [products](https://hestia.earth/schema/Cycle#products) with:
        - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) **or** [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) **or** [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
          - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [carbonContent](https://hestia.earth/term/carbonContent) **or** [nitrogenContent](https://hestia.earth/term/nitrogenContent) **or** [ligninContent](https://hestia.earth/term/ligninContent) and optional:
        - [startDate](https://hestia.earth/schema/Cycle#startDate)
        - a list of [products](https://hestia.earth/schema/Cycle#products) with:
          - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [discardedCropLeftOnField](https://hestia.earth/term/discardedCropLeftOnField) **or** [discardedCropIncorporated](https://hestia.earth/term/discardedCropIncorporated) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
            - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [carbonContent](https://hestia.earth/term/carbonContent) **or** [nitrogenContent](https://hestia.earth/term/nitrogenContent) **or** [ligninContent](https://hestia.earth/term/ligninContent)
        - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
          - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertiliser](https://hestia.earth/glossary?termType=organicFertiliser) **or** [soilAmendment](https://hestia.earth/glossary?termType=soilAmendment) and a list of [properties](https://hestia.earth/schema/Input#properties) with:
            - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [carbonContent](https://hestia.earth/term/carbonContent) **or** [nitrogenContent](https://hestia.earth/term/nitrogenContent) **or** [ligninContent](https://hestia.earth/term/ligninContent)
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [tillage](https://hestia.earth/glossary?termType=tillage)
          - [value](https://hestia.earth/schema/Practice#value) and [startDate](https://hestia.earth/schema/Practice#startDate) and [endDate](https://hestia.earth/schema/Practice#endDate) and a [term](https://hestia.earth/schema/Practice#term) with:
            - [termType](https://hestia.earth/schema/Term#termType) = [waterRegime](https://hestia.earth/glossary?termType=waterRegime) and [name](https://hestia.earth/schema/Term#name) containing "irrigated"

### Lookup used

- [tillage.csv](https://hestia.earth/glossary/lookups/tillage.csv) -> `IPCC_TILLAGE_CATEGORY`
- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`
- [measurement-model-siteTypesAllowed.csv](https://hestia.earth/glossary/lookups/measurement-model-siteTypesAllowed.csv)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('organicCarbonPerHa', Site))
```
