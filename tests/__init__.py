import os


CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
os.environ.setdefault('ECOINVENT_V3_FILEPATH', os.path.join(CURRENT_DIR, 'fixtures', 'ecoinventV3_excerpt.csv'))
